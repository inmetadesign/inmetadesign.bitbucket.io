/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

	console.log('Javascript up and running!');

	//Skriver current index på bildekarusell
	let slideIndex;

	//Kjører bare på sider mer produktkarusell
	var productImageCarouselWrapper = document.getElementById('product-images-wrapper');
	if(productImageCarouselWrapper) {
		
		var ul = document.querySelector('.product-images-wrapper__pagination ul');

		function changeActiveSlide() {
			slideIndex = this.currentSlide;
			liArray = ul.getElementsByTagName('li');
			for (var i = 0; i < liArray.length; i++) {
				liArray[i].classList.remove('active');
			}
			liArray[slideIndex].classList.add('active');	
		}

		const productImageCarousel = new Siema({
			selector: '.product-images',
			loop: true,
			onChange: changeActiveSlide
		});

		Siema.prototype.addPagination = function() {
			for (let i = 0; i < this.innerElements.length; i++) {
				const pagItem = document.createElement('li');
				
				pagItem.textContent = (i + 1);
				pagItem.addEventListener('click', () => this.goTo(i));
				document.querySelector('.product-images-wrapper__pagination ul').appendChild(pagItem);
			}
			liArray = ul.getElementsByTagName('li');
			liArray[0].classList.add('active');
		}

		productImageCarousel.addPagination();

		document.querySelector('.product-images-wrapper__navigation .browse-button--prev').addEventListener('click', () => productImageCarousel.prev());
		document.querySelector('.product-images-wrapper__navigation .browse-button--next').addEventListener('click', () => productImageCarousel.next());

		var productImagesCarouselInit = function() {
			if(window.innerWidth < 740) {
				productImageCarousel.init();
				console.log('karrusell');
			} else {
				productImageCarousel.destroy(true);
			}
		}

		productImagesCarouselInit();

		window.addEventListener("resize", function() {
			productImagesCarouselInit();
		});
	}

	//Legg i handlekurven
	$('.add-to-basket').on('click', function() {
		$(this).addClass('add-to-basket--added');
		$(this).text('Lagt til');
		$('.header .header__basket').addClass('product-added');
	});

	//Åpner/lukker produkt/filtermenyen
	$.fn.sortProducts = function(){
		/*console.log($(this));*/
		var $sortProducts = $(this);
		var $childMenu;

		/*var $categories = $sortProducts.find('.categories');*/
		var $childMenu = $sortProducts.find('.open-from-left');
		/*var $subcategoriesLi = $subcategories.find('>li');*/
		/*var $subcategoriesHeading = $childMenu.find('>ul>li>span');*/
		var $subcategoriesHeading = $childMenu.find('>ul>li>span');

		var $productGroup = $sortProducts.find('.product-group');
		var $productGroupHeading = $productGroup.find('>ul>li>span');

		var $dummyProductHeading = $('#dummy-product-heading');
		var clickedProductHeading;


		//Når man klikker på underkategori KLÆR, SKO osv
		$subcategoriesHeading.on('click', function() {
			console.log('gegege');

			$(this).parent('open-from-left').removeClass('closed');

			//Hvis man trykker på kategorimenyen når filtermenyen er åpen
			if($(this).parents('.open-from-left').hasClass('subcategory-menu')) {
				$('.filters-menu li.clicked').removeClass('clicked');
			}

			if($(this).parents('li').hasClass('clicked')) {
				$(this).parents('ul').find('li').removeClass('clicked');
				$(this).parents('li').removeClass('clicked');
			} else {
				$(this).parents('ul').find('li').removeClass('clicked');
				$(this).parents('li').addClass('clicked');
			}		
		});

		//Klikker på varegruppe KJOLE SKJØRT osv
		$productGroupHeading.on('click', function() {
			$childMenu.addClass('closed');
			$(this).parent('open-from-left').find('>li').removeClass('clicked');
			clickedProductHeading = $(this).text();

			//Bare for syns skyld
			$dummyProductHeading.text(clickedProductHeading);
		});
	}

	//Initsierer HEadroom.js, for å håndtere fancy scrollbackmeny (heter det det?)
	var fixedHEader  = document.getElementById("fixed-header");
	var searchCanvas  = document.getElementById("search-canvas");
	// construct an instance of Headroom, passing the element
	var headroomHeader  = new Headroom(fixedHEader, {
		"offset": 140,
		"tolerance": 2,
		"classes": {
			"initial": "animated",
			"pinned": "slideDown",
			"unpinned": "slideUp"
		}
	});
	// initialise
	headroomHeader.init();

	var headroomSearchCanvas  = new Headroom(searchCanvas, {
		"offset": 140,
		"tolerance": 2,
		"classes": {
			"initial": "animated",
			"pinned": "slideDown",
			"unpinned": "slideUp"
		}
	});
	// initialise
	/*headroomSearchCanvas.init();*/

	$('.sort-products').sortProducts();

	//Sorry jQuery :(

	//Åpner kategoier og filteremeny på mobil
	var mobileMenuInit = function() {
		var $clickedToggle;
		var clickedMenu;
		$('.toggle-open-from-left').on('click', function(e) {
			$clickedToggle = $(this);

			clickedMenu = $clickedToggle.attr('data-menu');
			$('.' + clickedMenu).toggleClass('closed');
		});
	}

	//Åpner/lukker søk
	$('.header__search-trigger, .search-canvas--close').on('click', function() {
		$('.search-canvas').toggleClass('open');
		$('.header__search-trigger').toggleClass('clicked');
	});


	//Gjør at label fjernes etter at man har skrevet noe inn i input

	function jumpyLabel() {
		(function() {
	// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
	if (!String.prototype.trim) {
		(function() {
			// Make sure we trim BOM and NBSP
			var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			String.prototype.trim = function() {
				return this.replace(rtrim, '');
			};
		})();
	}

	[].slice.call( document.querySelectorAll( '.jumpy-label .input' ) ).forEach( function( inputEl ) {
		console.log('input');
		// in case the input is already filled..
		if( inputEl.value.trim() !== '' ) {
			classie.add( inputEl.parentNode, 'jumpy-label--filled' );
		}

		// events:
		inputEl.addEventListener( 'focus', onInputFocus );
		inputEl.addEventListener( 'blur', onInputBlur );
	} );

	function onInputFocus( ev ) {
		classie.add( ev.target.parentNode, 'jumpy-label--filled' );
	}

	function onInputBlur( ev ) {
		if( ev.target.value.trim() === '' ) {
			classie.remove( ev.target.parentNode, 'jumpy-label--filled' );
		}
	}
	})();

	}

	jumpyLabel();

	//Blokken med tre "views", går igjen på mange sider. SORRY JQUERY :(
	var threeViewsBlock = document.querySelector('.block--three-views');

	if(threeViewsBlock) {
		var $viewPicker = $('.view-picker'),
			$viewTitles = $viewPicker.find('li span'),
			$viewFrame = $viewPicker.siblings('.view-frame').find('>div'),
			clickedViewId;
		$viewTitles.on('click', function() {
			$(this).parents('.view-picker').find('.active').removeClass('active');
			$(this).parents('li').addClass('active');
			clickedViewId = $(this).attr('data-view');
			$viewFrame.removeClass().addClass(clickedViewId);
		});
	}


	$(document).ready(function() {

		$.fn.editSearchQueries = function() {
			var $savedSearchQueries = $(this);
			var $editQueries = $savedSearchQueries.find('.admin-queries');
			var $editThisTitle = $savedSearchQueries.find('.query-list .name__edit-icon');

			$editQueries.on('click', function(e) {
				e.preventDefault();
				$savedSearchQueries.find('.name__edit-icon').toggleClass('visible');
				console.log($(this));
			})

			$editThisTitle.on('click', function() {
				$(this).parents('li').addClass('to-edit');
			})
			
		}
		
			
		

		$('.saved-search-queries').editSearchQueries();

		//Dette erstattes jo av noe React-greier du mekker, tipper jeg... Viser adresseskjemaet på utsjekk
		$('.user-buttons__unregistered').on('click', function() {
			$(this).parents('.delivery-information__user-buttons').fadeOut(500, function() {
				$('.fieldset--delivery-information').fadeIn(500);
			});
		});
		//"Fjerner" produkt i handlekurven
		$('.products-in-basket .delete .round-button--close').on('click', function(e) {
			e.preventDefault();
			$(this).parents('.product').fadeOut(500);
		});

		//Toggler klasse på box-buttons
		$('.box-buttons .button').on('click', function(e) {
			e.preventDefault();
			$(this).parent().find('.selected').removeClass('selected');
			$(this).toggleClass('selected');
		});


		var $footer = $('.footer');
		var $clickableFooterItem = $footer.find('.footer__menu>ul>li>span');

		$clickableFooterItem.on('click', function() {
			$(this).parent('li').toggleClass('clicked');
		});

		const mainCategoryMenu = new Siema({
			selector: '.nav--top-menu .categories',
			perPage: 4,
			loop: true,
			multipleDrag: true
		});

		//UL med kategoriene (dame, herre osv)
		var categoriesUlLis = document.querySelector('.nav--top-menu .categories').getElementsByTagName('li').length;
		
		if (categoriesUlLis < 4) {
			document.querySelector('.nav--top-menu .browse-button--prev').classList.add('hidden');
			document.querySelector('.nav--top-menu .browse-button--next').classList.add('hidden');
		}

		document.querySelector('.nav--top-menu .browse-button--prev').addEventListener('click', () => mainCategoryMenu.prev(3));
		document.querySelector('.nav--top-menu .browse-button--next').addEventListener('click', () => mainCategoryMenu.next(3));

		if(window.innerWidth < 741) {
			//Sjekker om bredden er smal nok til å lage scrollversjon av toppmenyen på sidelast
			mainCategoryMenu.init();

			//Initsierer mobilmenyen, hvor kategoriene kommer inn fra siden osv
			mobileMenuInit();
		} else {
			mainCategoryMenu.destroy(true);
			headroomSearchCanvas.init();
		}

		window.addEventListener("resize", function() {
			if(window.innerWidth < 740) {
				mobileMenuInit();
				mainCategoryMenu.init();
			} else {
				mainCategoryMenu.destroy(true);
				headroomSearchCanvas.init();
			}
		});

		//Viser "Totalsum oppdatert" når man ednrer fraktmåte
		$('.delivery-information .will-change-total').on('click', function() {
			$('.my-orders .total-changed').addClass('visible');
			setTimeout(function() {
					$('.my-orders .total-changed').removeClass('visible');
			}, 1800);
		});

		//Scroller til topp når man klikker på Totalsum oppdatert
		$('.total-changed').on('click', function() {
			$('html, body').animate({
		        scrollTop: ($("#my-orders").offset().top - 100)
		    }, 1000);
		});


		//Åpner/lukker totalsum i utsjekk
		$('.my-orders--total li.total').on('click', function() {
			$(this).toggleClass('clicked');
			$('.my-orders--summary').slideToggle(300);
		});



	});

/***/ })
/******/ ]);