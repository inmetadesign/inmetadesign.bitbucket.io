

if(document.querySelector('.js-collapse-sg-menu-button')) {
  const collapseSgMenuButton = document.querySelector('.js-collapse-sg-menu-button');
  
  const collapseSgMenuWrapper = document.querySelector('.js-collapse-sg-menu-wrapper');
  
  // Åpner/lukker mobilmeny på styleguide
  collapseSgMenuButton.onclick = function() {
    collapseSgMenuWrapper.classList.toggle('open');
  }
}

// Åpner/luker søkefelt på større skjermer
const searchButton = document.querySelectorAll('.js-expand-search-button');
const searchWrapper = document.querySelector('.js-expand-search-wrapper');

for (var i = 0; i < searchButton.length; i++) {
  searchButton[i].addEventListener("click", function() {
    searchWrapper.classList.toggle('open');
  });
}

const collapseBigMenuWrapper = document.querySelector('.js-collapse-big-menu-wrapper');
const collapseBigMenuButtons = document.querySelectorAll(".js-toggle-big-menu-button");

for (var i = 0; i < collapseBigMenuButtons.length; i++) {
  collapseBigMenuButtons[i].addEventListener("click", function() {
    collapseBigMenuWrapper.classList.toggle('open');
  });
}

// Trekkspill
var accordionTrigger = document.querySelectorAll('.js-accordion-trigger');
var accordionParent = document.querySelectorAll('.js-accordion-parent');

for (i = 0; i < accordionTrigger.length; i++) {
  accordionTrigger[i].addEventListener('click', toggleItem, false);
}
function toggleItem() {
  if (!this.parentNode.classList.contains('c_accordion__item--expanded')) {
    this.parentNode.classList.add('c_accordion__item--expanded');
    this.parentNode.classList.remove('c_accordion__item--closed');
  } else {
    this.parentNode.classList.remove('c_accordion__item--expanded');
    this.parentNode.classList.add('c_accordion__item--closed');
  }
}